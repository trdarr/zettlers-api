CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE IF NOT EXISTS human (
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  email character varying(256) NOT NULL,
  password character varying(60)  NOT NULL
);

CREATE TABLE IF NOT EXISTS session (
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  human uuid NOT NULL REFERENCES human,
  created timestamp NOT NULL DEFAULT current_timestamp
);

INSERT INTO human (id, email, password)
VALUES (
  '5b211102-a998-444d-b767-23adbee3e9c2', 'test',
  '$2b$12$fvdZ7qyURdts/Rzh6BFLA.pSBslEvaEgtGVq.xOIMzBllEs9j4UYW')
ON CONFLICT DO NOTHING;


INSERT INTO session (id, human, created)
VALUES (
  '9f214bca-81be-4571-91f7-0e6db2869779',
  '5b211102-a998-444d-b767-23adbee3e9c2', 'epoch')
ON CONFLICT DO NOTHING;
