# encoding: utf-8

import json
import unittest
import uuid

import zettlers


class LoginTestCase(unittest.TestCase):
  def setUp(self):
    self.app = zettlers.app.test_client()

  def test_log_in(self):
    response = self.app.post('/login',
        content_type='application/json',
        data=json.dumps({'email': 'test', 'password': 'test'}))
    self.assertEquals(200, response.status_code)

  def test_missing_email(self):
    response = self.app.post('/login',
        content_type='application/json',
        data=json.dumps({'password': 'test'}))
    self.assertEquals(400, response.status_code)

  def test_missing_email_and_password(self):
    response = self.app.post('/login')
    self.assertEquals(400, response.status_code)

  def test_missing_password(self):
    response = self.app.post('/login',
        content_type='application/json',
        data=json.dumps({'email': 'test'}))
    self.assertEquals(400, response.status_code)

  def test_wrong_password(self):
    response = self.app.post('/login',
        content_type='application/json',
        data=json.dumps({'email': 'test', 'password': 'production'}))
    self.assertEquals(400, response.status_code)


class RegisterTestCase(unittest.TestCase):
  def setUp(self):
    self.app = zettlers.app.test_client()

  def test_missing_email(self):
    response = self.app.post('/register',
        content_type='application/json',
        data=json.dumps({'password': 'test'}))
    self.assertEquals(400, response.status_code)

  def test_missing_email_and_password(self):
    response = self.app.post('/register')
    self.assertEquals(400, response.status_code)

  def test_missing_password(self):
    response = self.app.post('/register',
        content_type='application/json',
        data=json.dumps({'email': 'test'}))
    self.assertEquals(400, response.status_code)

  def test_reregister_email(self):
    response = self.app.post('/register',
        content_type='application/json',
        data=json.dumps({'email': 'test', 'password': 'test'}))
    self.assertEquals(400, response.status_code)

  def test_register(self):
    email = 'test-{}'.format(str(uuid.uuid4()))
    response = self.app.post('/register',
        content_type='application/json',
        data=json.dumps({'email': email, 'password': 'test'}))
    self.assertEquals(200, response.status_code)


class SessionsTestCase(unittest.TestCase):
  def setUp(self):
    self.app = zettlers.app.test_client()

  def test_missing_token(self):
    response = self.app.get('/sessions')
    self.assertEquals(400, response.status_code)

  def test_sessions(self):
    token = '9f214bca-81be-4571-91f7-0e6db2869779'
    response = self.app.get('/sessions?token={}'.format(token))
    self.assertEquals(200, response.status_code)

    sessions = json.loads(response.data.decode('utf-8'))
    self.assertGreaterEqual(len(sessions), 1)

  def test_unrecognized_token(self):
    response = self.app.get('/sessions?token={}'.format(str(uuid.uuid4())))
    self.assertEquals(400, response.status_code)

  def test_invalid_token(self):
    response = self.app.get('/sessions?token=blaha')
    self.assertEquals(400, response.status_code)
