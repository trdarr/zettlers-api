This depends on Python 3 and a reasonably recent version of Postgres.
On OS X, you can install them with Homebrew.

```bash
brew install postgresql python3
brew services start postgresql
createdb  # To create the superuser database.
pip install virtualenv
```

Some manual Postgres setup is required (`psql`).

```sql
CREATE USER zettlers WITH SUPERUSER;
CREATE DATABASE zettlers;
ALTER DATABASE zettlers OWNER TO zettlers;
```

Create the tables and insert some test data.

```bash
psql -U zettlers < schema.sql
```

Create a virtual environment, install the dependencies, and run the thing.

```bash
virtualenv -p python3 .venv
source .venv/bin/activate
python -m zettlers
```

You should have an application running on http://localhost:5002/.

You can run the tests from within the virtual environment with `nosetests`
