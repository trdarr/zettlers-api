# encoding: utf-8

import functools
import logging
import operator
import sys

import flask
import werkzeug.contrib.fixers
import werkzeug.exceptions
from flask import json, request, Response

from zettlers import postgres
from zettlers.users import UserRepository
from zettlers.util import SomeError


# Create the Flask application.
app = flask.Flask(__name__)
app.secret_key = 'lol'  # lol

connection_pool = postgres.create_connection_pool()


@app.before_first_request
def set_logger():
  if not app.debug:
    # Log everything, even when not in debug mode.
    app.logger.addHandler(logging.StreamHandler(sys.stdout))
    app.logger.setLevel(logging.INFO)

@app.before_request
def acquire_connection():
  flask.g.users = UserRepository(connection_pool.getconn())

@app.after_request
def release_connection(response):
  connection_pool.putconn(flask.g.users.connection)
  return response


@app.route('/login', methods=('POST',))
def login_view():
  try:
    # Extract the required fields from the request body.
    email, password = required_fields(request.get_json(), 'email', 'password')
  except SomeError as e:
    # If any required fields are missing, return an error.
    app.logger.info(str(e))
    return error_response(str(e))

  # Find a user with the given email address.
  user = flask.g.users.find_by_email(email)
  if user is None:
    # If the email address is unregistered, return an error.
    app.logger.info('Tried to log in with unregistered email address %s.', email)
    return error_response('Incorrect email address or password.')

  # Verify that the given password is correct.
  if not user.has_password(password):
    # If the password is incorrect, return an error.
    app.logger.info('Tried to log in with incorrect password for user %s.', user)
    return error_response('Incorrect email address or password.')

  # Create a new session for the user and return its id (the “token”).
  token = flask.g.users.log_in(user.id)
  app.logger.info('Created session %s for user %s.', token, user)
  return flask.jsonify({'token': token})


@app.route('/register', methods=('POST',))
def register_view():
  try:
    # Extract the required fields from the request body.
    email, password = required_fields(request.get_json(), 'email', 'password')
  except SomeError as e:
    # If any required fields are missing, return an error.
    app.logger.info(str(e))
    return error_response(str(e))

  # Find a user with the given email address.
  user = flask.g.users.find_by_email(email)
  if user is not None:
    # If the email address is already registered, return an error.
    app.logger.info('Tried to reregister email address %s.', email)
    return error_response('The email address is already registered.')

  # Register the user.
  user = flask.g.users.register(email, password)
  app.logger.info('Registered user %s.', user)

  # Create a new session for the user and return its id (the “token”).
  token = flask.g.users.log_in(user.id)
  app.logger.info('Created session %s for user %s.', token, user)
  return flask.jsonify({'token': token})


@app.route('/sessions')
def sessions_view():
  try:
    # Extract the required fields from the request body.
    token = required_fields(request.args, 'token')
  except SomeError as e:
    # If any required fields are missing, return an error.
    app.logger.info(str(e))
    return error_response(str(e))

  # Find a user with the given session id.
  user = flask.g.users.find_by_session_id(token)
  if user is None:
    # If there is no user with the given session id, return an error.
    app.logger.info('Tried to check activity for unrecognized session %s.', token)
    return error_response('Unrecognized session token %s.'.format(token))

  # Get the recent sessions for the user with the given session id.
  timestamps = flask.g.users.recent_sessions(user.id)
  return flask.json.dumps({'email': user.email, 'sessions': timestamps})


def error_response(message, status=400):
  response = flask.jsonify({'error': message})
  response.status_code = status
  return response


def required_fields(body, *fields):
  body = body or {}
  try:
    return operator.itemgetter(*fields)(body)
  except KeyError:
    message = 'Missing required field(s): {}.'
    missing_fields = filter(lambda f: f not in body, fields)
    raise SomeError(message.format(', '.join(missing_fields)))
