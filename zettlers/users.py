# encoding: utf-8

import hmac
import operator
import sys
import uuid
from collections import namedtuple
from datetime import datetime

import bcrypt

from zettlers import util


log = util.get_logger(__name__)


class UserRepository:
  def __init__(self, connection):
    self.connection = connection

  def find_by_email(self, email):
    '''Find the user with the given email address, or return None.'''

    with self.connection.cursor() as cursor:
      cursor.execute('SELECT * FROM human WHERE email = %s', (email,))
      self.connection.commit()
      return User.from_db(cursor.fetchone())

  def log_in(self, user_id):
    '''Create a new session for a user and return its id.'''

    session_id = str(uuid.uuid4())
    with self.connection.cursor() as cursor:
      sql = 'INSERT INTO session (id, human) VALUES (%s, %s)'
      cursor.execute(sql, (session_id, user_id))
      self.connection.commit()
      return session_id

  def register(self, email, password):
    '''Register a user with the given email address and password.'''

    user = User.create(email, password)
    with self.connection.cursor() as cursor:
      sql = 'INSERT INTO human (id, email, password) VALUES (%s, %s, %s)'
      cursor.execute(sql, (user.id, user.email, user.password))
      self.connection.commit()
    return user

  def find_by_session_id(self, session_id):
    '''Find the user with the given session id, or return None.'''

    try:
      uuid.UUID(session_id)
    except ValueError:
      return None

    with self.connection.cursor() as cursor:
      cursor.execute('''
        SELECT human.* FROM session
        JOIN human ON human.id = session.human
        WHERE session.id = %s
      ''', (session_id,))
      self.connection.commit()
      return User.from_db(cursor.fetchone())

  def recent_sessions(self, user_id, n=5):
    '''Find the n most recent sessions for the user with the given session id.'''

    try:
      if not isinstance(user_id, uuid.UUID):
        uuid.UUID(user_id)
    except ValueError:
      return []

    with self.connection.cursor() as cursor:
      cursor.execute('''
        SELECT created FROM session
        WHERE human = %s
        ORDER BY created DESC
        LIMIT %s
      ''', (user_id, n))
      self.connection.commit()
      return list(map(operator.itemgetter('created'), cursor.fetchall()))


class User(namedtuple('User', 'id email password')):
  def has_password(self, password):
    expected = bytes(self.password, 'utf-8')
    actual = bcrypt.hashpw(bytes(password, 'utf-8'), expected)
    return hmac.compare_digest(actual, expected)

  def __str__(self):
    return '%s <%s>' % (str(self.id)[:8], self.email)

  @staticmethod
  def create(email, password):
    password = bytes(password, 'utf-8')
    hashed_password = bcrypt.hashpw(password, bcrypt.gensalt()).decode('utf-8')
    return User(str(uuid.uuid4()), email, hashed_password)

  @staticmethod
  def from_db(result):
    if result is None:
      return None

    return User(result['id'], result['email'], result['password'])
