# encoding: utf-8

import psycopg2
import psycopg2.pool
import psycopg2.extras
import retrying

# Try to create a thread-safe PostgreSQL flask.g.db pool using a
# cursor factory that returns fetched rows as dictionaries instead of tuples.
# If the connection is refused, try a few times, waiting between tries.
def create_connection_pool():
  def is_operational_error(exception):
    return isinstance(exception, psycopg2.OperationalError)

  # Wait two seconds between tries. Try for ten seconds.
  @retrying.retry(retry_on_exception=is_operational_error,
      stop_max_delay=10 * 1000, wait_fixed=2 * 1000)
  def _create_connection_pool():
    return psycopg2.pool.ThreadedConnectionPool(1, 10, user='zettlers',
        cursor_factory=psycopg2.extras.RealDictCursor)

  return _create_connection_pool()

# Automatically convert between PostgreSQL’s `uuid` and Python’s `uuid.UUID`.
psycopg2.extras.register_uuid()
